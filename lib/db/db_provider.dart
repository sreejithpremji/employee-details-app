import 'dart:io';
import 'package:employee_details_app/models/employee_db_model.dart';
import 'package:employee_details_app/models/employee_model.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {
    // If database exists, return database
    if (_database != null) return _database;

    // If database don't exists, create one
    _database = await initDB();

    return _database;
  }

  // Create the database and the Employee table
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'employee.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE Employee('
          'id INTEGER PRIMARY KEY,'
          'email TEXT,'
          'name TEXT,'
          'address TEXT,'
          'companyName TEXT,'
          'profileImage TEXT,'
          'phoneNo TEXT,'
          'companyCatchPhrase TEXT,'
          'website TEXT,'
          'companyBs TEXT,'
          'userName TEXT'
          ')');
    });
  }

  // Insert employee on database
  Future<void> createEmployee(Employee newEmployee) async {
    // await deleteAllEmployees();
    final db = await database;
    await db.insert('Employee', newEmployee.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  // Delete all employees
  Future<int> deleteAllEmployees() async {
    final db = await database;
    final res = await db.rawDelete('DELETE FROM Employee');
    return res;
  }

  check() async {
    final db = await database;
    final res = await db.rawQuery(
        "SELECT name FROM sqlite_master WHERE type='table' AND name='Employee'");
    return res;
  }

  Future<List<Employee>> getAllEmployees() async {
    final db = await database;
    final res = await db.rawQuery("SELECT * FROM EMPLOYEE");
    List<Employee> list =
        res.isNotEmpty ? res.map((c) => Employee.fromJson(c)).toList() : [];
    return list;
  }
}
