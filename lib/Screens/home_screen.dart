import 'package:employee_details_app/Screens/details_screen.dart';
import 'package:employee_details_app/models/employee_db_model.dart';
import 'package:employee_details_app/network/api_manager.dart';
import 'package:employee_details_app/providers/employee_provider.dart';
import 'package:employee_details_app/utils/size_config.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  APIManager apiManager = APIManager();
  String searchFieldText = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<EmployeeProvider>(context, listen: false).getEmployee(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Container(
              width: double.infinity,
              height: SizeConfig.getHeight(context),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: SizeConfig.getHeight(context) * 0.2,
                    child: Center(
                      child: TextField(
                        decoration: InputDecoration(
                            hintText: 'Search', suffixIcon: Icon(Icons.search)),
                        onChanged: (String value) {
                          setState(() {
                            searchFieldText = value;
                            Provider.of<EmployeeProvider>(context,listen: false).handleSearch(context, value);
                          });
                        },
                      ),
                    ),
                  ),
                  Consumer<EmployeeProvider>(
                      builder: (context, provider, child) {
                    return provider.isLoading
                        ? CircularProgressIndicator()
                        : (provider.employee.isEmpty
                            ? Container()
                            : buildEmployeeList(searchFieldText.isNotEmpty ||
                                    provider.searchResult.length != 0
                                ? provider.searchResult
                                : provider.employee));
                  }),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildEmployeeList(List<Employee> employee) {
    return Container(
      height: SizeConfig.getHeight(context) * 0.7,
      child: ListView.builder(
          shrinkWrap: true,
          physics: ScrollPhysics(),
          itemCount: employee.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailsScreen(
                                employee: employee[index],
                              )));
                },
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.black)),
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          height: SizeConfig.getHeight(context) * 0.2,
                          width: SizeConfig.getWidth(context) * 0.3,
                          child: Image.network(employee[index].profileImage),
                        ),
                      ),
                      Column(
                        children: [
                          Text(
                            employee[index].name,
                            style: TextStyle(color: Colors.blue),
                          ),
                          SizedBox(
                            height: SizeConfig.getHeight(context) * 0.015,
                          ),
                          Text(employee[index].companyName),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            );
          }),
    );
  }
}
