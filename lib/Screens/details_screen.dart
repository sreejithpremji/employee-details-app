import 'dart:convert';
import 'package:employee_details_app/models/employee_db_model.dart';
import 'package:employee_details_app/utils/size_config.dart';
import 'package:flutter/material.dart';

class DetailsScreen extends StatefulWidget {
  final Employee employee;

  const DetailsScreen({Key key, this.employee}) : super(key: key);

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  Map<String, dynamic> address;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    address = jsonDecode(widget.employee.address);
    print(address);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            height: SizeConfig.getHeight(context),
            width: SizeConfig.getWidth(context),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: SizeConfig.getHeight(context) * 0.2,
                    width: SizeConfig.getWidth(context) * 0.3,
                    child: Image.network(widget.employee.profileImage),
                  ),
                  SizedBox(
                    height: SizeConfig.getHeight(context) * 0.1,
                  ),
                  buildDetails("Name", widget.employee?.name ?? ""),
                  SizedBox(
                    height: SizeConfig.getHeight(context) * 0.03,
                  ),
                  buildDetails("User name", widget.employee?.userName ?? ""),
                  SizedBox(
                    height: SizeConfig.getHeight(context) * 0.03,
                  ),
                  buildDetails("Email", widget.employee?.email ?? ""),
                  SizedBox(
                    height: SizeConfig.getHeight(context) * 0.03,
                  ),
                  buildDetails("Phone", widget.employee?.phoneNo ?? ""),
                  SizedBox(
                    height: SizeConfig.getHeight(context) * 0.03,
                  ),
                  buildDetails("Website", widget.employee?.website ?? ""),
                  SizedBox(
                    height: SizeConfig.getHeight(context) * 0.03,
                  ),
                  buildDetails(
                      "Company Name", widget.employee?.companyName ?? ""),
                  SizedBox(
                    height: SizeConfig.getHeight(context) * 0.03,
                  ),
                  buildDetails("Company Phrase",
                      "'${widget.employee?.companyCatchPhrase}'" ?? ""),
                  SizedBox(
                    height: SizeConfig.getHeight(context) * 0.03,
                  ),
                  buildDetails("Company BS", widget.employee?.companyBs ?? ""),
                  SizedBox(
                    height: SizeConfig.getHeight(context) * 0.03,
                  ),
                  Flexible(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            width: SizeConfig.getWidth(context) * 0.25,
                            child: Text("Address :")),
                        SizedBox(width: SizeConfig.getWidth(context) * 0.07),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Flexible(
                                child: Text(
                              "${address["street"] ?? ""},",
                              overflow: TextOverflow.ellipsis,
                            )),
                            SizedBox(
                              height: SizeConfig.getHeight(context) * 0.01,
                            ),
                            Flexible(
                                child: Text(
                              "${address["suite"] ?? ""},",
                              overflow: TextOverflow.ellipsis,
                            )),
                            SizedBox(
                              height: SizeConfig.getHeight(context) * 0.01,
                            ),
                            Flexible(
                                child: Text(
                              "${address["city"] ?? ""},",
                              overflow: TextOverflow.ellipsis,
                            )),
                            SizedBox(
                              height: SizeConfig.getHeight(context) * 0.01,
                            ),
                            Flexible(
                                child: Text(
                              "${address["zipcode"] ?? ""}",
                              overflow: TextOverflow.ellipsis,
                            )),
                            SizedBox(
                              height: SizeConfig.getHeight(context) * 0.01,
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildDetails(String title, String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
            width: SizeConfig.getWidth(context) * 0.25,
            child: Text("$title :")),
        SizedBox(width: SizeConfig.getWidth(context) * 0.07),
        Flexible(
            child: Text(
          value,
          overflow: TextOverflow.ellipsis,
        )),
      ],
    );
  }
}
