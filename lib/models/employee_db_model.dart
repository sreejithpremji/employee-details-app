class Employee {
  int id;
  String name;
  String profileImage;
  String companyName;
  String email;
  String userName;
  String phoneNo;
  String companyCatchPhrase;
  String website;
  String companyBs;
  String address;

  Employee(
      {this.companyBs,
      this.companyCatchPhrase,
      this.companyName,
      this.email,
      this.id,
      this.name,
      this.phoneNo,
      this.profileImage,
      this.userName,
      this.website,
      this.address});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['userName'] = this.userName;
    data['email'] = this.email;
    data['profileImage'] = this.profileImage;
    data['address'] = this.address;
    data['phoneNo'] = this.phoneNo;
    data['website'] = this.website;
    data['companyBs'] = this.companyBs;
    data['companyCatchPhrase'] = this.companyCatchPhrase;
    data['companyName'] = this.companyName;
    return data;
  }

  factory Employee.fromJson(Map<String, dynamic> json) => Employee(
      id: json['id'],
      name: json['name'],
      userName: json['userName'],
      email: json['email'],
      profileImage: json['profileImage'],
      address: json['address'],
      phoneNo: json['phoneNo'],
      website: json['website'],
      companyName: json['companyName'],
      companyCatchPhrase: json['companyCatchPhrase'],
      companyBs: json["companyBs"]);
}
