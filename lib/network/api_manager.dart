import 'dart:convert';
import 'dart:developer';

import 'package:employee_details_app/models/employee_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

class APIManager {
  Future<List<EmployeeModel>> getEmployeeDetails(BuildContext context) async {
    List<EmployeeModel> result;
    Uri url = Uri.parse("http://www.mocky.io/v2/5d565297300000680030a986");
    try {
      final response = await http.get(url);
      // print("****************");
      // print(response.statusCode);
      // print(response.body);
      // print("****************");

      if (response.statusCode == 200) {
        result = employeeModelFromJson(response.body);
      } else {
        Fluttertoast.showToast(
            msg: "Some thing went wrong..",
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIosWeb: 1,
            backgroundColor: Colors.black,
            textColor: Colors.white,
            fontSize: 16.0);
      }
    } catch (e) {
      print("Exception $e");
    }
    // print("result is $result");
    return result;
  }
}
