import 'dart:convert';
import 'package:employee_details_app/db/db_provider.dart';
import 'package:employee_details_app/models/employee_db_model.dart';
import 'package:employee_details_app/models/employee_model.dart';
import 'package:employee_details_app/network/api_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class EmployeeProvider extends ChangeNotifier {
  APIManager apiManager = APIManager();
  List<EmployeeModel> responseList = [];
  List<Employee> searchResult = [];
  List<Employee> employee = [];
  bool isSearching = false;
  bool isLoading = false;

  getEmployee(BuildContext context) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    bool isDataFetchedAlready =
        preferences.getBool("isDataFetchedAlready") ?? false;
    if (!isDataFetchedAlready) {
      isLoading = true;
      responseList = await apiManager.getEmployeeDetails(context);
      isLoading = false;
      if (responseList.isNotEmpty) {
        List<Employee> employeeList = [];
        for (int i = 0; i < responseList.length; i++) {
          employeeList.add(Employee(
              address: jsonEncode(responseList[i].address),
              companyBs: responseList[i].company?.bs ?? "",
              companyCatchPhrase: responseList[i].company?.catchPhrase ?? "",
              companyName: responseList[i].company?.name ?? "",
              email: responseList[i].email,
              id: responseList[i].id,
              name: responseList[i].name,
              phoneNo: responseList[i].phone.toString(),
              profileImage: responseList[i].profileImage.toString(),
              userName: responseList[i].username.toString(),
              website: responseList[i].website));
        }
        for (int j = 0; j < employeeList.length; j++) {
          DBProvider.db.createEmployee(employeeList[j]);
        }
        preferences.setBool("isDataFetchedAlready", true);
      }
    }
    employee = await DBProvider.db.getAllEmployees();
    print(jsonEncode(employee));
    notifyListeners();
  }

  handleSearch(BuildContext context, String value) {
    searchResult.clear();
    if (!isSearching) {
      isSearching = true;
      for (int i = 0; i < employee.length; i++) {
        String data = employee[i].name;
        if (data.toLowerCase().contains(value.toLowerCase())) {
          addData(employee[i]);
        }
      }
      isSearching = false;
    }
  }

  addData(Employee value) {
    searchResult.add(value);
    notifyListeners();
  }
}
